package TodosLosMenu;

import java.util.ArrayList;

import Pojo.Empleados;

public class Menus {

    public static void menu_servicios_main() {
        System.out.println("1. Registrar empleado");
        System.out.println("2. Borrar empleado");
        System.out.println("3. Ver nómina");
        System.out.println("4. Salir");
    }

    public static void HorasExtras() {
        System.out.println("Trabajó horas extras?");
        System.out.println("1. Si");
        System.out.println("2. No");
    }

    public static void header() {
        System.out.format("%3s %20s %15s %15s %7s %7s %12s %8s %8s %12s %15s %15s \n%1s \n", "Id", "Nombre", "Cargo",
                "Salario Bruto", "INNS L", "IR", "Salario neto", "INNS P", "INATEC", "Vacaciones", "Treceavo Mes",
                "Indemnizacion",
                "-----------------------------------------------------------------------------------------------------------------------------------------------------------------");
    }

    public static void print(ArrayList<Empleados> empleados) {
        header();
        for (Empleados e : empleados) {
            System.out.format("%3s %20s %15s %15.2f %10.2f %7.2f %12.2f %8.2f %8.2f %12.2f %15.2f %15.2f \n",e.getId(),e.getNombre(),e.getCargo(),e.getSalario_devengado(),e.getINSS_L(),e.getIR(),e.getSalario_neto(),e.getINSS_P(),e.getINATEC(),e.getVacaciones(),e.getTreceavo_mes(),e.getIndemnizacion());
        }
    }

}

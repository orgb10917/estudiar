package Servicios;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import Implementaciones.Nomina_Implement;
import Pojo.Empleados;
import TodosLosMenu.Menus;

public class Nomina_Services implements IServices<Empleados>{
    private Scanner sc;
    private Nomina_Implement Nm;


    public Nomina_Services(Scanner sc) throws IOException{
        this.sc=sc;
        Nm = new Nomina_Implement();
	}

	@Override
    public void create() throws IOException {
        int id;
        String nombre;
        String cargo;
        double salario_devengado;
        double INSS_L;
        double IR;
        double INSS_P;
        double INATEC;
        double salario_neto;
        double vacaciones;
        double treceavo_mes;
        double indemnizacion;
        double salario_horas;
        double salario_basico;

        System.out.println("Ingrese un ID para el empleado: ");
        id = sc.nextInt();

        System.out.println("Ingrese el nombre del empleado: ");
        nombre = sc.next();


        System.out.println("Ingrese el cargo del empleado: ");
        cargo = sc.nextLine();
        
        sc.nextLine();
        System.out.println("Ingrese el salario por hora que ganará el empleado: ");
        salario_horas = sc.nextDouble();
        salario_basico = salario_horas * 240;
        salario_devengado = salario_basico;

        int opc_menu_HorasExtras;

        do{
            Menus.HorasExtras();
            opc_menu_HorasExtras = sc.nextInt();

            switch (opc_menu_HorasExtras) {
                case 1:
                    System.out.println("¿Cuantas horas extras trabajo?");
                    double horas_Ex = sc.nextDouble();
                    salario_devengado += (horas_Ex * salario_horas);

                    break;
                
                case 2: 

                    break;

                default:
                    System.out.println("Opción incorrecta, favor ingresar la opción nuevamente:");
                    break;
            }

        }while(opc_menu_HorasExtras<=0 || opc_menu_HorasExtras>=3);

        //Deducciones
        INSS_L = salario_devengado * 0.07;
        IR = calculoIR(salario_devengado - INSS_L);
        salario_neto = salario_devengado - INSS_L - IR; // Salario neto =  salario devengado - Deducciones

        //Prestaciones sociales
        INSS_P = salario_devengado * 0.215;
        INATEC = salario_devengado * 0.02;
        vacaciones = salario_devengado * 0.08333;
        treceavo_mes = vacaciones;
        indemnizacion = vacaciones;

        Empleados e = new Empleados(id, nombre, cargo, salario_devengado, INSS_L, IR, INSS_P, INATEC, salario_neto, vacaciones, treceavo_mes, indemnizacion);

        Nm.create(e);

        System.out.println("Empleado guardado exitosamente!!!!");
    }

    private double calculoIR(double salario_devengado) {
        double IR = 0;
        if (salario_devengado >= 0.01 && salario_devengado <= 8333.33){
            return IR;
        }else if(salario_devengado > 8333.33 && salario_devengado <= 16666.66){
            IR = (salario_devengado - 8333.33) * 0.15;
            return IR;
        }else if(salario_devengado > 16666.66 && salario_devengado <= 29166.66){
            IR = (salario_devengado-16666.66) * 0.20 + 1250;
            return IR;
        }else if(salario_devengado > 29166.66 && salario_devengado <= 41666.66){
            IR = (salario_devengado-29166.66) * 0.25 + 3750;
            return IR;
        }else if(salario_devengado > 41666.66){
            IR = (salario_devengado-41666.66) * 0.30 + 6875;
        }
        return IR;
    }

    @Override
    public void delete() throws IOException {
        int id;
        Empleados e;
        System.out.println("Ingrese el ID del empleado a eliminar: ");
        id = sc.nextInt();

        e = Nm.FindById(id); 

        if (e == null){
            System.out.println("Empleado no encontrado");
        }else{
            Nm.delete(e);
            System.out.println("El empleado se ha borrado con éxito!!!");
        }
    }

    @Override
    public void mostrarNomina() throws IOException {
        ArrayList<Empleados> e = Nm.mostrarNomina();
        Menus.print(e);
        System.out.println("");
    }

}


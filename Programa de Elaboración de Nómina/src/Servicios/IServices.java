package Servicios;

import java.io.IOException;

public interface IServices <T>{
    void create() throws IOException;
    void delete() throws IOException;
    void mostrarNomina() throws IOException;
}

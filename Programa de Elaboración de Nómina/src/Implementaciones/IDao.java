package Implementaciones;

import java.util.ArrayList;
import java.io.IOException;

public interface IDao <T>{
    void create(T t) throws IOException;
    void delete(T t) throws IOException;
    ArrayList<T> mostrarNomina() throws IOException;
}

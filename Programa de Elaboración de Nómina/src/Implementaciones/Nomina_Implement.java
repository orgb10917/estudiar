package Implementaciones;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import Pojo.Empleados;

public class Nomina_Implement implements IDao<Empleados>{
	private ArrayList <Empleados>  empleado = new ArrayList<Empleados>();
	private Empleados empleados;

	@Override
	public void create(Empleados t) throws FileNotFoundException, IOException {
		empleado.add(t);
	}

	@Override
	public void delete(Empleados t) throws FileNotFoundException, IOException {
		empleado.remove(t);
	}

	public Empleados FindById(int id_buscar) throws IOException {

		empleados=null;

		for(int i=0; i<empleado.size();i++){
			if(id_buscar==empleado.get(i).getId()){
				
				empleados = empleado.get(i);
				
			}
		}

		return empleados;
	}

	@Override
	public ArrayList<Empleados> mostrarNomina() throws IOException {
		return empleado;
	}
    
}

import java.io.IOException;
import java.util.Scanner;
import Servicios.Nomina_Services;

public class Main {
    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);

        Nomina_Services nServices = new Nomina_Services(sc);

        int opc;
        boolean salir=true;
        
        do{
            TodosLosMenu.Menus.menu_servicios_main();
            opc = sc.nextInt();

            switch (opc) {
                case 1:
                    nServices.create();
                    break;

                case 2: 
                    nServices.delete();
                    break;

                case 3: 
                    nServices.mostrarNomina();
                    break;

                case 4:
                    System.out.println("Gracias por usar la APP!!!!");
                    System.out.println("Vuelva pronto.");
                    salir = false; 

                    break;

                default:
                    System.out.println("Opción incorrecta, favor ingresar su opción nuevamente: ");
                    break;
            }

        }while(salir);
    }
}